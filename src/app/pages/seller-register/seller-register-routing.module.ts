import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SellerRegisterPage } from './seller-register.page';

const routes: Routes = [
  {
    path: '',
    component: SellerRegisterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SellerRegisterPageRoutingModule {}
