import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductPreviewCartPage } from './product-preview-cart.page';

const routes: Routes = [
  {
    path: '',
    component: ProductPreviewCartPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductPreviewCartPage]
})
export class ProductPreviewCartPageModule {}
