export class Login {
    // tslint:disable-next-line: variable-name
    _id = Number;
    email: string;
    password: string;
}

export class BuyerRegister {
    fname: string;
    lname: string;
    state: string;
    country: 'country';
    companyName: string;
    roleInCompany: string;
    companyWebsite: string;
    phoneNumber: string;
    email: string;
    password: string;
    registrationDate: Date;
}

export class SellerRegister {
    fname: string;
    lname: string;
    gender: string;
    country: 'country';
    phoneNumber: string;
    email: string;
    password: string;
    registrationDate: Date;
}
