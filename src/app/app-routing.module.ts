import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  // tslint:disable-next-line: max-line-length
  { path: 'buyer-register', loadChildren: () => import('./pages/buyer-register/buyer-register.module').then( m => m.BuyerRegisterPageModule) },
  { path: 'buyer-success', loadChildren: () => import('./pages/buyer-success/buyer-success.module').then( m => m.BuyerSuccessPageModule)},
  // tslint:disable-next-line: max-line-length
  { path: 'create-account', loadChildren: () => import('./pages/create-account/create-account.module').then( m => m.CreateAccountPageModule) },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)},
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)},
  { path: 'tabs', loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule)},
  // tslint:disable-next-line: max-line-length
  { path: 'product-preview', loadChildren: () => import('./pages/product-preview/product-preview.module').then(m => m.ProductPreviewPageModule)},
  { path: 'seller-home', loadChildren: () => import('./pages/seller-home/seller-home.module').then( m => m.SellerHomePageModule)},
  // tslint:disable-next-line: max-line-length
  { path: 'seller-success', loadChildren: () => import('./pages/seller-success/seller-success.module').then( m => m.SellerSuccessPageModule)},
  // tslint:disable-next-line: max-line-length
  { path: 'seller-register', loadChildren: () => import('./pages/seller-register/seller-register.module').then( m => m.SellerRegisterPageModule) },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
