import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerHomePage } from './seller-home.page';

describe('SellerHomePage', () => {
  let component: SellerHomePage;
  let fixture: ComponentFixture<SellerHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SellerHomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SellerHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
