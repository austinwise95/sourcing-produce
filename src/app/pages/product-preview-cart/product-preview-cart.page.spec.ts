import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPreviewCartPage } from './product-preview-cart.page';

describe('ProductPreviewCartPage', () => {
  let component: ProductPreviewCartPage;
  let fixture: ComponentFixture<ProductPreviewCartPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPreviewCartPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPreviewCartPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
