import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { Login, BuyerRegister, SellerRegister } from '../models/authentication';
// import { url } from 'inspector';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  // API path
  baseLoginPath = 'http://source-produce.herokuapp.com/buyer/login';
  buyerBasePath = 'https://source-produce.herokuapp.com/buyer/register';
  sellerBasePath = 'https://source-produce.herokuapp.com/seller/register';
  proApi = 'https://source-produce.herokuapp.com/buyer/productOffers';

  constructor(private http: HttpClient) { }

  // http options
  httpOptions = {
    headers: new HttpHeaders({
      // 'Content-Type': 'application/x-www-form-urlencoded'
      'Content-Type': 'application/json'
    })
  };

  // Handle errors from api
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // clientside or network error, handle accordingly
      console.error('Error occured', error.error.message);
    } else {
      // backend returned and unsuccessful response code
      // response body may contain clues as to what went wrong
      console.error(
        `Backend returned code ${error.status}, ` +
        `response body was ${error.error}`
      );
    }
    // return an observable with user facing error message
    return throwError(
      'Something bad happened; please try again later'
    );
  }

  private extractData(res: Response) {
    // tslint:disable-next-line: prefer-const
    let body = res;
    return body || {};
  }

  // login in buyer
  login(body) {
  // tslint:disable-next-line: max-line-length
  const headerOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

    // tslint:disable-next-line: max-line-length
  return this.http.post(this.baseLoginPath + '?email=' + body.email + '&password=' + body.password, headerOptions ).pipe(retry(2), catchError(this.handleError));
  }

  // Register Buyer
  buyerRegister(buyerBody: BuyerRegister) {
    return this.http.post(this.buyerBasePath, buyerBody, this.httpOptions)
    .pipe(retry(2), catchError(this.handleError));
  }

  // Register Seller
  sellerRegister(sellerBody: SellerRegister) {
    return this.http.post(this.sellerBasePath, sellerBody, this.httpOptions)
    .pipe(retry(2), catchError(this.handleError));
  }

  // productOffers
  productOffers(): Observable<any> {
    return this.http.get(this.proApi, this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  // productPreview
  productPreview(productName: string): Observable<any> {
    return this.http.get(this.proApi + '/' + productName, this.httpOptions).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }
}
