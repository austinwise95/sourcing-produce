import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.page.html',
  styleUrls: ['./create-account.page.scss'],
})
export class CreateAccountPage implements OnInit {

  hideBuyer: boolean;
  hideSeller: boolean;

  progress = 0.05;

  constructor(public location: Location, public navCtrl: NavController) { }

  ngOnInit() {
  }

  hide1() {
    this.hideBuyer = true;
    this.hideSeller = false;
    this.progress = this.progress + 0.1;

    document.getElementById('buyer').style.background = 'white';
    document.getElementById('buyer').style.color = '#66AF08';
    document.getElementById('buyer').style.outline = 'none';
    document.getElementById('buyer').style.borderRadius = '5px 0px 0px 5px';
    document.getElementById('buyer').style.border = '1px solid #E5E5E5';
    document.getElementById('seller').style.background = '#EDF2F7';
    document.getElementById('seller').style.color = '#CBD5E0';
    document.getElementById('seller').style.border = '1px solid #E5E5E5';
    document.getElementById('seller').style.borderRadius = '0px 5px 5px 0px';
    document.getElementById('seller').style.outline = 'none';
  }

  hide2() {
    this.hideSeller = true;
    this.hideBuyer = false;
    this.progress = this.progress + 0.1;

    document.getElementById('seller').style.background = 'white';
    document.getElementById('seller').style.color = '#66AF08';
    document.getElementById('seller').style.outline = 'none';
    document.getElementById('seller').style.borderRadius = '0px 5px 5px 0px';
    document.getElementById('seller').style.border = '1px solid #E5E5E5';
    document.getElementById('buyer').style.background = '#EDF2F7';
    document.getElementById('buyer').style.color = '#CBD5E0';
    document.getElementById('buyer').style.outline = 'none';
    document.getElementById('buyer').style.border = '1px solid #E5E5E5';
    document.getElementById('buyer').style.borderRadius = '5px 0px 0px 5px';
  }

  continueToBuyer() {
    this.navCtrl.navigateForward('/buyer-register');
  }

  continueToSeller() {
    this.navCtrl.navigateForward('/seller-register');
  }

  goBack() {
    this.location.back();
  }

  cancel() {
    this.navCtrl.navigateBack('/home');
  }
}
