import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSlides } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.page.html',
  styleUrls: ['./product-preview.page.scss'],
})

export class ProductPreviewPage implements OnInit {
  @ViewChild('mySlider', {static: false}) slider: IonSlides;

  data: any;
  slidesOpts = {
    autoplay: true,
      speed: 500,
      zoom: {
        maxRatio: 5
      },
  };

  constructor(public location: Location, private apiService: ApiService, private activatedroute: ActivatedRoute) { }

  products: [];

  ngOnInit() {
    this.activatedroute.queryParams.subscribe({
      next: params => {
        if (params && params.special) {
          this.data = JSON.parse(params.special);
          console.log(this.data);
        }
        console.log(params);
      },
      error: err => {
        console.log(err);
        this.location.back();
      }
    });
  }

  goBack() {
    this.location.back();
  }

}
