import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BuyerRegisterPage } from './buyer-register.page';

const routes: Routes = [
  {
    path: '',
    component: BuyerRegisterPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BuyerRegisterPageRoutingModule {}
