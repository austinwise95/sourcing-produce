import * as core from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { Login } from '../../models/authentication';
import { ToastService } from 'src/app/services/toast.service';

@core.Directive()
@core.Directive()
@core.Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements core.OnInit {

  data: Login;
  loginResponse: any;

  constructor(
    public navCtrl: NavController,
    public alert: AlertController,
    private apiService: ApiService,
    public router: Router,
    private toastService: ToastService
    ) {
    this.data = new Login();
    this.loginResponse = {};
  }

  ngOnInit() {
  }

  login() {
    this.apiService.login(this.data).subscribe((response) => {
      console.log(response);
      this.loginResponse = response;
      localStorage.setItem('fname', this.loginResponse.fname);
      localStorage.setItem('lname', this.loginResponse.lname);
      localStorage.setItem('email', this.loginResponse.email);
      localStorage.setItem('state', this.loginResponse.state);
      localStorage.setItem('country', this.loginResponse.country);
      this.router.navigate(['/tabs']);
    },
    (error: any) => {
      console.log(error);
      // this.toastService.presentToast('Incorrect username and password.');
    });
  }
}


