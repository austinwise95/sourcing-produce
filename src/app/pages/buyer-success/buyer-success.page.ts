import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-buyer-success',
  templateUrl: './buyer-success.page.html',
  styleUrls: ['./buyer-success.page.scss'],
})
export class BuyerSuccessPage implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

}
