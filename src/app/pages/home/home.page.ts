import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('mySlider', {static: false}) slider: IonSlides;

  slidesOpts = {
    autoplay: false,
      speed: 500,
      // zoom: {
      //   maxRatio: 5
      // },
  };

  constructor(public navCtrl: NavController) {}

  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit() {
  }

  login() {
    this.navCtrl.navigateForward('/login');
  }
  createAcc() {
    this.navCtrl.navigateForward('/create-account');
  }

}
