import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductOfferPage } from './product-offer.page';

describe('ProductOfferPage', () => {
  let component: ProductOfferPage;
  let fixture: ComponentFixture<ProductOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductOfferPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
