import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { SellerRegister } from 'src/app/models/authentication';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-seller-register',
  templateUrl: './seller-register.page.html',
  styleUrls: ['./seller-register.page.scss'],
})
export class SellerRegisterPage implements OnInit {

  sellerBody: SellerRegister;

  constructor(public location: Location, public navCtrl: NavController, private apiService: ApiService, public router: Router) {
    this.sellerBody = new SellerRegister();
  }

  countries: any[] = [
    {
      id: 1,
      country: 'Niger'
    },
    {
      id: 2,
      country: 'Nigeria'
    },
    {
      id: 3,
      country: 'Ghana',
    },
    {
      id: 4,
      country: 'Mozambique',
    },
    {
      id: 5,
      country: 'Namibia',
    },
    {
      id: 6,
      country: 'Rwanda',
    },
    {
      id: 7,
      country: 'Togo',
    },
    {
      id: 8,
      country: 'Senegal',
    }
  ];

  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }
  // tslint:disable-next-line: member-ordering
  compareWith = this.compareWithFn;

  yesButton() {
    document.getElementById('yesBt').style.width = '89px';
    document.getElementById('yesBt').style.color = '#fff';
    document.getElementById('yesBt').style.background = '#66AF08';
    document.getElementById('yesBt').style.paddingLeft = '6px';
    document.getElementById('yesBt').style.border = '1px solid #F3F3F3';
    document.getElementById('yesBt').style.outline = 'none';
    document.getElementById('yesBt').innerHTML = 'yes' + '<ion-icon name="checkmark" slot="end"></ion-icon>';
    document.getElementById('noBt').style.width = '40px';
    document.getElementById('noBt').style.outline = 'none';
    document.getElementById('noBt').style.paddingRight = '5px';
    document.getElementById('noBt').innerHTML = 'No';
  }

  noButton() {
    document.getElementById('noBt').style.width = '65px';
    document.getElementById('noBt').style.color = '#DADADA';
    document.getElementById('noBt').innerHTML = 'Yes';
    document.getElementById('noBt').style.outline = 'none';
    document.getElementById('yesBt').style.background = '#F44336';
    document.getElementById('yesBt').style.outline = 'none';
    document.getElementById('yesBt').style.color = '#fff';
    document.getElementById('yesBt').style.width = '65px';
    document.getElementById('yesBt').style.paddingLeft = '5px';
    document.getElementById('yesBt').innerHTML = 'No';
  }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  sellerRegister() {
  console.log(this.sellerBody);
  this.apiService.sellerRegister(this.sellerBody).subscribe((response) => {
      console.log(response);
      if (response !== 'Data Successfully Captured. You can now proceed to login') {
        console.log(response);
      } else {
        document.getElementById('submit').style.background = '66AF08';
        document.getElementById('submit').style.color = 'white';
        document.getElementById('submit').style.outline = 'none';
        this.router.navigate(['/seller-success']);
      }
    });
  }
}
