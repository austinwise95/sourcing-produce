import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-seller-success',
  templateUrl: './seller-success.page.html',
  styleUrls: ['./seller-success.page.scss'],
})
export class SellerSuccessPage implements OnInit {

  constructor( private router: Router) { }

  ngOnInit() {
  }

  sellerHome() {
    this.router.navigate(['/seller-home']);
  }
}
