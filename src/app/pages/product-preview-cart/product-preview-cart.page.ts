import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController, IonSlides } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-product-preview-cart',
  templateUrl: './product-preview-cart.page.html',
  styleUrls: ['./product-preview-cart.page.scss'],
})
export class ProductPreviewCartPage implements OnInit {

  @ViewChild('mySlider', {static: false}) slider: IonSlides;

  slidesOpts = {
    autoplay: true,
      speed: 500,
      zoom: {
        maxRatio: 5
      },
  };

  private id: string;

  constructor(public location: Location) { }

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  // tslint:disable-next-line: one-line
  isAddedToCart(id): boolean{
    return id === this.id;
  }

  addToCart(id) {
    this.id = id;
  }
}
