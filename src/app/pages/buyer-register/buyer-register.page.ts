import * as core from '@angular/core';
import { Location } from '@angular/common';
import { NavController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { BuyerRegister } from 'src/app/models/authentication';
import { ApiService } from '../../services/api.service';

@core.Directive()
@core.Directive()
@core.Component({
  selector: 'app-buyer-register',
  templateUrl: './buyer-register.page.html',
  styleUrls: ['./buyer-register.page.scss'],
})

export class BuyerRegisterPage implements core.OnInit {

  buyerBody: BuyerRegister;

  // tslint:disable-next-line: max-line-length
  constructor(public location: Location, public navCtrl: NavController, private apiService: ApiService, public alert: AlertController, public router: Router
  ) {
    this.buyerBody = new BuyerRegister();
  }

  countries: any[] = [
    {
      id: 1,
      country: 'Niger'
    },
    {
      id: 2,
      country: 'Nigeria'
    },
    {
      id: 3,
      country: 'Ghana',
    },
    {
      id: 4,
      country: 'Mozambique',
    },
    {
      id: 5,
      country: 'Namibia',
    },
    {
      id: 6,
      country: 'Rwanda',
    },
    {
      id: 7,
      country: 'Togo',
    },
    {
      id: 8,
      country: 'Senegal',
    }
  ];

  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }
  // tslint:disable-next-line: member-ordering
  compareWith = this.compareWithFn;

  ngOnInit() {
  }

  goBack() {
    this.location.back();
  }

  buyerRegister() {
   console.log(this.buyerBody);
   this.apiService.buyerRegister(this.buyerBody).subscribe((response: Response) => {
      console.log(response);
      if (response.msg !== 'Data Successfully Captured. You can now proceed to login') {
        console.log(response);
      } else {
        document.getElementById('submit').style.background = '66AF08';
        document.getElementById('submit').style.color = 'white';
        document.getElementById('submit').style.outline = 'none';
        this.router.navigate(['/buyer-success']);
      }
    });
  } 
}

export interface Response {
  msg;
}
