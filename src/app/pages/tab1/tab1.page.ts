import { NavController, IonSlides } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from '../../services/api.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit {

  constructor(private router: Router, private apiService: ApiService) { }

  cart = [];
  productOffer: [];
  firstName: string;
  lastName: string;
  email: string;
  state: string;
  country: string;

  @ViewChild('mySlider', { static: false }) slider: IonSlides;
  @ViewChild('prodSlide', { static: false }) slider2: IonSlides;

  slidesOpts = {
    autoplay: true,
    speed: 500,
    zoom: {
      maxRatio: 5
    },
  };

  // tslint:disable-next-line: member-ordering
  slidesOpts2 = {
    slidesPerView: 2,
    autoplay: true,
    speed: 1500,
    zoom: {
      maxRatio: 5
    },
  };

  ngOnInit() {
    this.firstName = localStorage.getItem('fname');
    this.lastName = localStorage.getItem('lname');
    this.email = localStorage.getItem('email');
    this.state = localStorage.getItem('state');
    this.country = localStorage.getItem('country');
    this.productOffers();
  }

  async productOffers() {
    await this.apiService.productOffers()
      .subscribe(res => {
        console.log(res);
        this.productOffer = res;
        console.log(this.productOffer);
      }, err => {
        console.log(err);
      });
  }

  async productPreview(productName: string) {
    await this.apiService.productPreview(productName)
      .subscribe(res => {
        const navigationExtras: NavigationExtras = {
          queryParams: {
            special: JSON.stringify(res)
          }
        };
        this.router.navigate(['/product-preview'], navigationExtras);
      }, err => {

      });
  }

  viewAll() {
    this.router.navigate(['tabs/tabs/product-offer']);
  }

}
