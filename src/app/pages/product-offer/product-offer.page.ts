import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { AlertController, IonItemSliding } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-product-offer',
  templateUrl: './product-offer.page.html',
  styleUrls: ['./product-offer.page.scss'],
})

export class ProductOfferPage implements OnInit {

  User: any;
  productOffer: [];
  fullProductOffer: [];

  // tslint:disable-next-line: max-line-length
  constructor( public location: Location, public alertController: AlertController, private apiService: ApiService, private router: Router) { }

  goBack() {
    this.location.back();
  }

  ngOnInit() {
    this.productOffers();
    const productFilters = document.getElementsByClassName('productFilter');
    for (let index = 0; index < productFilters.length; index++) {
      const productFilter = productFilters.item(index);
      productFilter.addEventListener('click', () => {
        const innerTxt = productFilter.innerHTML;
        // tslint:disable-next-line: prefer-const
        let filteredProducts: any = [];
        // tslint:disable-next-line: prefer-for-of
        for (let j = 0; j < this.fullProductOffer.length; j++) {
          const product: Products = this.fullProductOffer[j];
          console.log(product);
          const check = product.productName.includes(innerTxt);
          if (check) {
            filteredProducts.push(product);
          }
          console.log(check);
        }
        this.productOffer = filteredProducts;
      });
    }
    const searchbar = document.querySelector('ion-searchbar');
    searchbar.addEventListener('ionInput', async (event) => {
      const query = event.target.value;
      // const query = event.target.addEventListener;
      const filteredProducts: any = [];
      await this.apiService.productOffers()
      .subscribe(res => {
        requestAnimationFrame(() => {
          res.forEach(item => {
            const check = item.productName.includes(query);
            if (check) {
                filteredProducts.push(item);
              }
          });
          this.productOffer = filteredProducts;
        });
      }, err => {
        console.log(err);
      });

    });
  }

  async productOffers() {
    await this.apiService.productOffers()
      .subscribe(res => {
        console.log(res);
        this.fullProductOffer = res;
        this.productOffer = this.fullProductOffer;
        console.log(this.productOffer);
      }, err => {
        console.log(err);
      });
  }

  async productPreview(productName: string) {
    await this.apiService.productPreview(productName)
      .subscribe(res => {
        const navigationExtras: NavigationExtras = {
          queryParams: {
            special: JSON.stringify(res)
          }
        };
        this.router.navigate(['/product-preview'], navigationExtras);
      }, err => {

      });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Let’s help you with what you are looking for!',
      inputs: [
        {
          name: 'name1',
          type: 'text',
          placeholder: 'Placeholder 1'
        },
        {
          name: 'name2',
          type: 'text',
          id: 'name2-id',
          value: 'hello',
          placeholder: 'Placeholder 2'
        },
        {
          name: 'name3',
          value: 'http://ionicframework.com',
          type: 'url',
          placeholder: 'Favorite site ever'
        },
        // input date with min & max
        {
          name: 'name4',
          type: 'date',
          min: '2017-03-01',
          max: '2018-01-12'
        },
        // input date without min nor max
        {
          name: 'name5',
          type: 'date'
        },
        {
          name: 'name6',
          type: 'number',
          min: -5,
          max: 10
        },
        {
          name: 'name7',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: () => {
            console.log('Confirm Ok');
          }
        }
      ]
    });
    await alert.present();
  }

}

export interface Products {
  _id: any;
  productName: any;
  imageName: any;
}
